//
//  Pin.swift
//  Mapas
//
//  Created by Master Móviles on 31/3/17.
//  Copyright © 2017 Master Móviles. All rights reserved.
//

import Foundation
import MapKit

class Pin: NSObject, MKAnnotation {
    var coordinate: CLLocationCoordinate2D
    var title: String?
    var subtitle: String?
    var thumbImage: UIImage?
    
    init(num: Int, coordinate: CLLocationCoordinate2D) {
        self.title = "Pin \(num)"
        self.subtitle = "Un bonito lugar"
        self.coordinate = coordinate
        if (num % 2 == 0) {
            self.thumbImage = UIImage(named: "Alicante.png")!
        } else {
            self.thumbImage = UIImage(named: "alicante-2.png")!
        }
        super.init()
    }
}
