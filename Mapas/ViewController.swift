//
//  ViewController.swift
//  Mapas
//
//  Created by Master Móviles on 31/3/17.
//  Copyright © 2017 Master Móviles. All rights reserved.
//

import UIKit
import MapKit

enum TipoMapa: Int {
    case Mapa = 0
    case Satélite
}

class ViewController: UIViewController, MKMapViewDelegate {

    var numPin = 0
    
    @IBOutlet weak var mapView: MKMapView! {
        didSet {
            mapView.mapType = .standard
            mapView.delegate = self
            
            let alicanteLocation = CLLocationCoordinate2D(latitude: 38.3453, longitude: -0.4831)
            let initialLocation = CLLocation(latitude: alicanteLocation.latitude, longitude: alicanteLocation.longitude)
            
            centerMapOnLocation(mapView: mapView, loc: initialLocation)
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        let userTrackingButton = MKUserTrackingBarButtonItem(mapView: mapView)
        self.navigationItem.leftBarButtonItem = userTrackingButton
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Actions
    
    @IBAction func onMapTypeChanged(_ sender: Any) {
        let segmented = sender as! UISegmentedControl
        let type = TipoMapa(rawValue: segmented.selectedSegmentIndex)!
        
        changeMapType(type)
    }
    
    @IBAction func onPinClicked(_ sender: Any) {
        let pin = Pin(num: numPin, coordinate: mapView.centerCoordinate)
        mapView.addAnnotation(pin)
        numPin += 1
    }

    // MARK: - Class Methods
    
    func centerMapOnLocation(mapView: MKMapView, loc: CLLocation) {
        let regionRadius: CLLocationDistance = 1000
        let coordinateRegion =
            MKCoordinateRegionMakeWithDistance(loc.coordinate,
                                               regionRadius * 4.0,
                                               regionRadius * 4.0)
        mapView.setRegion(coordinateRegion, animated: true)
    }
    
    func changeMapType(_ type: TipoMapa) {
        switch (type) {
        case .Mapa:
            mapView.mapType = .standard
        case .Satélite:
            mapView.mapType = .satellite
        }
    }
    
    //MARK: - Map Delegate Methods
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if let pin = annotation as? Pin {
            let view = MKPinAnnotationView(annotation: annotation, reuseIdentifier: nil)
            view.pinTintColor = UIColor.red
            view.animatesDrop = true
            view.canShowCallout = true
        
            let thumbnailImageView = UIImageView(frame: CGRect(x:0, y:0, width: 59, height: 59))
            thumbnailImageView.image = pin.thumbImage
        
            view.leftCalloutAccessoryView = thumbnailImageView
            view.rightCalloutAccessoryView = UIButton(type: .detailDisclosure)
        
            return view;
        }
        return nil
    }
    
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
        performSegue(withIdentifier: "imageDetail", sender: view)
    }
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "imageDetail" {
            if let pin = (sender as? MKAnnotationView)?.annotation as? Pin {
                if let vc = segue.destination as? ImageDetailViewController {
                    vc.image = pin.thumbImage
                }
            }
        }
    }
    
}

